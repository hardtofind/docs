# Order Management

<aside class="warning">
Although the API enables you to easily import Order data into your in-house system, it is still a requirement that
you include an official hardtofind packing slip within each of your shipments.

A direct download link is provided within the <code>_links.packing_slip</code> element to allow you to download the PDF packing slip directly.
</aside>

## Standard Order structure

```json
{
    "id": "123456",
    "status": "READY",
    "_links": {
        "self": "https://api.hardtofind.com.au/order/123456",
        "manage": "https://www.hardtofind.com.au/mfadmin/sales_order_part/view/123456",
        "packing_slip": "https://api.hardtofind.com.au/order/123456/packing-slip"
    },
    "dates": {
        "created_at": "2016-08-18T14:43:22+1000",
        "updated_at": "2016-08-18T14:43:22+1000",
        "dispatch_due_at": "2016-08-23T12:18:00+1000"
    },
    "payment_summary": {
        "price": 99.95,
        "shipping": 0,
        "value_addition": 0,
        "discount": 0,
        "credit": 0,
        "payment": 99.95,
        "total": 99.95
    },
    "lines": [
        {
            "description": "Product title here",
            "price": 99.95,
            "product_id": "111",
            "quantity": 1,
            "sku": "SKU-111",
            "variant_data": [
                {
                    "label": "Clothing size",
                    "value": "XL"
                }
            ],
            "personalisations": [
                {
                    "label": "First personalisation",
                    "value": "Something personal"
                },
                {
                    "label": "Second personalisation",
                    "value": "Something even more personal"
                }
            ]
        }
    ],
    "gift_wrapping": {
        "description": "Free gift wrap with personal message",
        "message": "Have a lovely time!",
        "price": 0,
        "quantity": 1
    },
    "shipping": {
        "address": {
            "first_name": "Tim",
            "last_name": "Massey",
            "line_one": "12 Address Street",
            "line_two": "",
            "suburb": "Suburbia",
            "state": "ACT",
            "postcode": "1001",
            "country": "Australia",
            "company": "Company Name"
        },
        "method": {
            "description": "Shipping - FREE standard shipping",
            "price": 0,
            "quantity": 1
        },
        "instructions": "If no-one home - please leave on front step."
    },
    "shipments": [
        {
            "shipment_date": "2016-09-08T14:57:37+1000",
            "tracking_number": "TRACK-1234",
            "shipping_provider_name": "Provider Name Here"
        }
    ]
}
```

The JSON structure to the right is a representation of an Order within the system and should provide you with all the data
necessary to fulfill the order.

## Order States

The available Order states within hardtofind.com.au are as follows:

Order State | Description
----------- | -----------
READY | The Order has been paid for fully and is awaiting fulfillment by the Seller
PROCESSED | The Order has been picked up by the Seller and is undergoing fulfillment
SHIPPED | The Order items have been fully shipped by the Seller.
RETURNED | The Order has been returned in full
REFUNDED | The Order has had the monies refunded to the Shopper
CANCELLED | The Order has been cancelled by the Customer Support team and the finances manually corrected.

## GET /orders

To view a list of Orders use this endpoint. It is possible to filter the list based on the following query string parameters.

By default the status of `PENDING` will be returned. Orders are ordered by last updated date (descending), most recently updated first.

Orders will be returned using the standard paged data structure as outlined [here](#paged-data-pagination).

Query parameter | Available options | Description
--------------- | ----------------- | ------------
status | One of the status listed [here](#order-states) | When supplied, only orders matching the status will be returned.
page_number | <code>[0-9]</code> | To be used for paging through large result sets

## GET /order/{id}

To retrieve a specific Order, issue a GET request to this endpoint. The return structure will match the standard Order as outlined [here](#order-management)

## POST /order/{id}/transition

```json
{
    "action": "<action name>",
    "data":{}
}
```

To move an order through its lifecycle, create a transition using this endpoint.

Available actions are:

* process
* ship

### Process transition

```json
{
    "action": "process"
}
```

The process transition only requires an action property of process.

### Ship transition

```json
{
    "action": "ship",
    "data":{
        "tracking_number": "TRACK-123456",
        "shipping_provider_name": "Royal Mail",
        "shipment_date": "2016-09-15T14:41:25+1000"
    }
}
```

The structure of a shipment transition requires an action of ship and the data contains the shipment provider and tracking information. For a list of valid shipping providers please use `GET /shipping-providers`

## GET /shipping-providers

To retrieve a list of the current Shipping Providers, issue a GET request to this endpoint. A listing of the current providers will be returned as follows.

```json
[
    {
        "id": "8",
        "provider_name": "Royal Mail",
        "tracking_url": "https://www.royalmail.com/track-your-item",
        "accepts_tracking_id_in_url": false
    }
]
```
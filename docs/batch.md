# Batch operations

For easy management of frequently changing data and to facilitate keeping your in-house systems in sync with hardtofind.com.au
a series of batch update jobs can be created. They are outlined below.

## Variant inventory updates:

To update variant inventory, send a request to `POST /batch` with the JSON as outlined here within the body.

```json
{
   "action":"update",
   "entity_type": "variant",
   "entities":[
      {
         "sku":"<SKU-001>",
         "inventory": 199
      },
      {
         "sku":"<SKU-002>",
         "inventory": 99
      },
      {
         "sku":"<SKU-003>",
         "inventory": 55
      }
   ]
}
```

If the supplied SKU's are not unique within your catalog, they will be ignored. We recommed ensuring your SKU's are
unique within the system in any case. This business rule is not yet enforced, but may be in future.

If the supplied SKU's do not exist within your catalog, they will be ignored.